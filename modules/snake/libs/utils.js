const pickRandomFromArray = (array) => {
  const prop = Math.floor(Math.random() * array.length)
  return array[prop]
}

export {pickRandomFromArray}