

const renderGridCell = () => {
  const el = document.createElement('div')
  el.classList = ['grid__cell']
  return el
}
const renderGrid = (settings) => {
  const { el, size = 10 } = settings
  const dimension = Math.pow(size, 2)
  
  el.innerHTML = ''

  for (let i = 1; i <= dimension; i++) {
    el.appendChild(renderGridCell())
  }
}

const resetGrid = () => {
  const gridCells = document.querySelectorAll('.grid .grid__cell')
  gridCells.forEach((x) => x.innerHTML = '')
}

export {renderGrid, resetGrid}