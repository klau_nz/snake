import { renderGrid, resetGrid } from './libs/grid.js'
import { pickRandomFromArray } from './libs/utils.js'
import { isValid } from './libs/rules.js'

export default function(el) {
  /** variables */
  let score = 0
  let snakePosition = [0, 1, 2]
  let direction = 'right'
  const start = document.querySelector('.start')
  const button = document.querySelectorAll('.controls > button')
  const foodItems = ['🍒', '🍌', '🍎']
  const grid = {
    el: document.querySelector(el),
    size: 10,
  }

  /** Create grid cells */
  renderGrid(grid)
  const gridCells = document.querySelectorAll('.grid__cell')

  /** functions */
  const spawnFood = () => {
    const cell = pickRandomFromArray(gridCells)
    const food = pickRandomFromArray(foodItems)
    cell.innerHTML = food
  }

  const renderSnake = () => {
    gridCells.forEach((x) => {
      x.style.backgroundColor = 'transparent'
      x.classList.remove('snake')
    })

    for (const i of snakePosition) {
      const head = snakePosition.slice(-1)[0]
      const body = snakePosition.slice(-2)[0]
      gridCells[i].style.backgroundColor = '#dedede'
      gridCells[i].classList.add('snake')
      gridCells[body].innerText = ''
      gridCells[head].innerText = '👀'
    }
  }

  const move = (direction) => {
    const newPos = snakePosition?.slice(-1)
    const num = {
      right: newPos[0] + 1,
      left: newPos[0] - 1,
      up: newPos[0] - grid.size,
      down: newPos[0] + grid.size
    }
    snakePosition.shift()
    snakePosition.push(num[direction])
    renderSnake()
    if (isValid(snakePosition)) {
    } else clearInterval(endLoop)
  }

  const startGame = () => {
    snakePosition = [0, 1, 2]
    resetGrid()
    spawnFood()
    const startLoop = setInterval(() => {
      move(direction)
    }, 700)
  }

  const endLoop = () => {
    
  }

  start.addEventListener('click', startGame)

  button.forEach((x) => {
    x.addEventListener('click', (x) => {
      direction = x.target.dataset.direction
    })
  })

}